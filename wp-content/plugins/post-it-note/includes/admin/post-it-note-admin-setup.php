<?php


//Prevent User from deactivating plugin
add_filter( 'plugin_action_links', 'disable_plugin_deactivation', 10, 4 );
	function disable_plugin_deactivation( $actions, $plugin_file, $plugin_data, $context ) {
	// Remove edit link for all plugins
	if ( array_key_exists( 'edit', $actions ) )
	unset( $actions['edit'] );
	// Remove deactivate link for important plugins
	if ( array_key_exists( 'deactivate', $actions ) && in_array( $plugin_file, array(
	'post-it-note/post-it-note.php'
	)))
	unset( $actions['deactivate'] );
	return $actions;
 }


//Setup Custom Post Type
add_action( 'init', 'register_custom_post_types' );

function register_custom_post_types(){
    $customPostTypeArgs = array(
                'label'=>'Post It Notes',
                'labels'=>
                    array(
                        'name'=>'Post It Notes',
                        'singular_name'=>'Post It Note',
                        'add_new'=>'Add Post It Note',
                        'add_new_item'=>'Add New Post It Note',
                        'edit_item'=>'Edit Post It Note',
                        'new_item'=>'New Post It Note',
                        'view_item'=>'View Post It Note',
                        'search_items'=>'Search Post It Notes',
                        'not_found'=>'No Post It Note Found',
                        'not_found_in_trash'=>'No Post It Notes Found in Trash',
                        'menu_name' => 'All Post It Notes',
                        'name_admin_bar'     => 'Post It Notes',
                    ),
                'public'=>true,
                'has_archive' => true,
                'slug' => 'post-it-list',
                'description'=>'All Post It Notes', 
                'exclude_from_search'=>false,
                'show_ui'=>true,
                'supports'=>array('title','thumbnail', 'custom_fields'),
                'taxonomies'=>array('category','post_tag'));
     
    
    register_post_type( 'notes', $customPostTypeArgs );
}