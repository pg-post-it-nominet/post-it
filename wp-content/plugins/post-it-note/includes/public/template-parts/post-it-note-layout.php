
<?php 

    //ACF fields
    require plugin_dir_path(__FILE__) . '/post-it-acf-mapping.php';

?>

<div class="col-md-4 animateUp">  
    <div class="postItNote">
        <?php if( $post_it_priority){ ?>
            <p class="postItNote__priority--<?php echo esc_attr($post_it_priority);?>"> Priority: <?php echo esc_html($post_it_priority);?></p>
        <?php } ?>

        <?php if($post_it_title){ ?>
            <h4 class="postItNote__title"><?php echo esc_html($post_it_title);?></h4>
        <?php } ?>

        <?php if($post_it_date){ ?>
            <p class="postItNote__dueDate">Due: <?php echo esc_html($pretty_due_date);?> </p>
        <?php } ?>

        <?php if( $post_it_short_description){ ?>
            <p><?php echo  esc_html($post_it_short_description);?> </p>
        <?php } ?>
 
        
        <?php if( $post_it_long_description){ ?>
            <button type="button" class="postItNote__button" data-toggle="modal" data-target="#modal-<?php echo get_the_ID() ?>">
                Read More Detail
            </button>
        <?php } ?>
        
    </div>
</div>


<?php //Create Bootstrap modal

if( $post_it_long_description){ ?>

<div class="modal fade" id="modal-<?php echo get_the_ID() ?>" tabindex="-1" role="dialog" aria-labelledby="<?php echo get_the_ID() ?>ModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <?php if($post_it_title){ ?>
                    <h3 class="modal-title" id="<?php echo get_the_ID() ?>ModalLabel"><?php echo esc_html($post_it_title);?></h3>
                <?php } ?>
                
                <?php if( $post_it_priority){ ?>
                    <p class="postItNote__priority--<?php echo esc_attr($post_it_priority);?>"> Priority: <?php echo esc_html($post_it_priority);?></p>
                 <?php } ?>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <?php if($post_it_date){ ?>
                    <em>Due: <?php echo esc_html($pretty_due_date)?> </em>
                <?php } ?>
                <?php if( $post_it_short_description){ ?>
                    <h4><?php echo  esc_html($post_it_short_description);?> </h4>
                <?php } ?>  

                <?php if( $post_it_long_description){ ?>
                    <p><?php echo wp_kses_post($post_it_long_description);?><p> 
                <?php } ?>
                <a class="postItNote__link" title="link to details" href="<?php the_permalink(); ?>">Link to detail page</a>
            </div>
            <div class="modal-footer">
                <button type="button" class="postItNote__button" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<?php } ?>