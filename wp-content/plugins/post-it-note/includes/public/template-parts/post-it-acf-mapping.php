<?php //Setup ACF fields for templating
 $post_it_title = get_field ('post_it_title');
 $post_it_date = get_field ('post_it_date'); //stored as Ymd for post sorting 
 $post_it_short_description = get_field ('post_it_short_description');
 $post_it_long_description = get_field ('post_it_long_description', false, false); //prevents wrapping of p tags
 $post_it_priority = get_field ('post_it_priority');

  //Pretty date format
  $date = new DateTime($post_it_date);
  $pretty_due_date = $date->format('d M Y');
  
  ?>