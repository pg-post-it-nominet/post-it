<?php


//Create single and archive pages inside plugin based on custom post type name.

//Archive template for post it notes
add_filter( 'archive_template', 'override_archive_template' );
function override_archive_template( $archive_template ){
     global $post;

     $file = dirname(__FILE__) .'/archive-'. $post->post_type .'.php';

     if( file_exists( $file ) ) $archive_template = $file;
     
     return $archive_template;
}



//Single template for post it notes
add_filter( 'single_template', 'override_single_template' );
function override_single_template( $single_template ){
    global $post;

    $file = dirname(__FILE__) .'/single-'. $post->post_type .'.php';

    if( file_exists( $file ) ) $single_template = $file;

    return $single_template;
}

//Enqueue Supporting Scripts & Styles
function enqueue_supporting_script_styles(){
    wp_enqueue_style('related-styles', plugins_url('/css/post-it-note.css', __FILE__));
    wp_enqueue_script('releated-script', plugins_url( '/js/post-it-note.js' , __FILE__ ));
}
add_action('wp_enqueue_scripts','enqueue_supporting_script_styles');