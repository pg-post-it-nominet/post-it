<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @since 1.0.0
 */

get_header();

//Setup Custom Loop
$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
$args = array(
    'post_type' => 'notes',
    'posts_per_page' => -1,
    'paged' => $paged,
    'orderby' => 'meta_value_num',
    'meta_key'  => 'post_it_date', //Soonest notes due first
    'order' => 'ASC',
);
$loop = new WP_Query( $args ); ?>

<div class="container">

    <div class="row justify-content-center">
        <div class="col">
            <div class="priorityKey">
                    Priority: <span class="priorityKey__high">High</span> <span class="priorityKey__medium">Medium</span> <span class="priorityKey__low">Low</span>
            </div>
        </div>
        
    </div>


    <div class="row">

        <?php while ( $loop->have_posts() ) : $loop->the_post(); 
        
                require plugin_dir_path(__FILE__) . '/template-parts/post-it-note-layout.php';
        
        endwhile; ?>

    </div>

</div>

<?php

wp_reset_postdata(); 

get_footer(); ?>