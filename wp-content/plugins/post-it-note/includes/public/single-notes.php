<?php
/**
 * The template for single 
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @since 1.0.0
 */
//ACF fields are stored here and variables used for legibile template
require plugin_dir_path(__FILE__) . '/template-parts/post-it-acf-mapping.php';

get_header();?>

    <div class="container">
        <div class="row">    
            <div class="col">
                <?php if($post_it_title){ ?>
                    <h1><?php echo esc_html($post_it_title);?> </h1>
                <?php } ?>
                <div class="row justify-content-between">
                    <?php if($post_it_date){ ?>
                        <div class="col-4"><em>Due Date:</em> <strong><?php echo esc_html($pretty_due_date);?></strong> </div>
                    <?php } ?>
                    <?php if( $post_it_priority){ ?>
                        <div class="col-4"><em>Priority:</em> <strong><?php echo esc_html($post_it_priority);?></strong> </div>
                    <?php } ?>
                </div>
                <?php if( $post_it_short_description){ ?>
                    <h3><?php echo  esc_html($post_it_short_description);?> </h3>
                <?php } ?>
                <?php if( $post_it_long_description){ ?>
                    <p><?php echo wp_kses_post($post_it_long_description);?> </p>
                <?php } ?>
                
            </div>
        </div>  
        <div class="row justify-content-center">
                <a class="postItNote__button" href="<?php echo get_post_type_archive_link( 'notes' ); ?>">See All Notes</a>
        </div> 
    </div>

<?php get_footer(); ?>