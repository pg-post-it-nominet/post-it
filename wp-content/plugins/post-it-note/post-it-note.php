<?php

/**
 * 
 * @link              hooksolutions.co.uk
 * @since             1.0.0
 * @package           Post_It
 *
 * @wordpress-plugin
 * Plugin Name:       Nominet Plugin: Post It Note!
 * Plugin URI:        hooksolutions.co.uk
 * Description:       Post It Note generator, uses a custom post type.
 * Version:           1.0.0
 * Author:            Phil Greeno
 * Author URI:        hooksolutions.co.uk
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       post-it
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'POST_IT_VERSION', '1.0.0' );

//Includes Admin and Public functions
require_once plugin_dir_path(__FILE__) . 'includes/admin/post-it-note-admin-setup.php';
require_once plugin_dir_path(__FILE__) . 'includes/public/post-it-note-public-setup.php';
