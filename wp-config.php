<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'post-it' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'OK:$0$VB&iHhn;TAGS )4EVN6TD55}gqqzHNB3ve+,.7p-Sr.RQqa??p*lF0N>5U' );
define( 'SECURE_AUTH_KEY',  '!L[>8T@YGCNv[p 2PpJN5npQl+pja0!;i+$4jY:_N98U-zQDsJ6SAG_g o@g_.w0' );
define( 'LOGGED_IN_KEY',    '>fT=DWa~sU2ZISBwc#zUpn,;JHW:d(wpgmN];T<l+*P;D-L`!C^m8J!Iu[`Y)eB,' );
define( 'NONCE_KEY',        'L;k1;mY6?i{I-9(U/9n:JkdDCt7o(3S7q4KN/@mI[NY_X>el%SJR_ 1%V=}8y3+k' );
define( 'AUTH_SALT',        'C*=d&)9x1BLx<P!WkMNl}vZ13egizd W2 ?_j2E#*z_R$%N--YI?k9RJD[P;3<Ia' );
define( 'SECURE_AUTH_SALT', 'Y};)<GN(sW{&AjMV*_EaKQUwdB0XL>ay!1 rgRpuW/FY>g gwd{up*axoGOIx^0i' );
define( 'LOGGED_IN_SALT',   'WUiIC.^L;S4*~0J$b]A6eQX=mL)3Z:Za6zG2*E;DVnsXBlCp.9l=wM#c|n[(9<EW' );
define( 'NONCE_SALT',       'Ed;J#6Z=l@w /`[3Q/JANFD>+L;d2KG(o)yRlMOabg-,3zl?R:3{@-*jL02{glzW' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', true );
define( 'WP_DEBUG_LOG', true );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
